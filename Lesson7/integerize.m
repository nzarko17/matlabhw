function res = integerize(A)
m = max(max(A));
if m <= intmax('uint8')
    res = 'uint8';
elseif m <= intmax('uint16')
    res = 'uint16';
elseif m <= intmax('uint32')
    res = 'uint32';
elseif m<= intmax('uint64')
    res = 'uint64';
else
    res = 'NONE';
end
