function a = randomness(limit,n,m)
a = zeros(n,m);
i = 1:m*n;
tmp = floor(1 + (limit).*rand(n*m,1));
a(i) = tmp(i);
end