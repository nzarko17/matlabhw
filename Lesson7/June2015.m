function res = June2015
res = cell (30,3);

% Create a list (vector) with days names :
days_name = ['Mon'; 'Tue'; 'Wed'; 'Thu';'Fri';'Sat';'Sun'];

%initialize the 1st column with June
res(:,1) = {'June'};
for jj = 0:29
    res{jj+1,2} = jj+1;
    res{jj+1,3} = days_name(mod(jj,7) + 1,:);
end

