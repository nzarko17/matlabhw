function [s1, s2, sums] = sines(pts, amp, f1, f2)
if nargin == 0 % nothing provided 
    pts = 1000;
    amp = 1.0;
    f1 = 100;
    f2 = 105;
elseif nargin == 1 % pts provided
    amp = 1;
    f1 = 100;
    f2 = 105;
elseif nargin == 2 % pts, amp provided
    f1 = 100;
    f2 = 105;
elseif nargin == 3 % pts, amp, f1 provided
    f2 = 1.05*f1;
end 

step1 = 2*pi*f1 / (pts - 1);
step2 = 2*pi*f2 / (pts - 1);
vec1 = 0:step1:2*f1*pi;
vec2 = 0:step2:2*f2*pi;
s1 = amp*sin(vec1);
s2 = amp*sin(vec2);
sums = s1 + s2;