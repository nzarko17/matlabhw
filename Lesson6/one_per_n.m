function res = one_per_n(n)
ss = 0;
count = 0;
while (ss < n) && (count <= 10000)
    count = count + 1;
    ss = ss + 1 /count;        
end

if (count) > 10000
    res = -1;
else
    res = count;
end
