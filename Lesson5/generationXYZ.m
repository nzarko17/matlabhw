function gen = generationXYZ(n)
if nargin < 1 
    error('Function takes only one argument!');
end
if n <= 0 || n ~= fix(n)
    error('Function argument must be a positive integer!');
end
if nargout > 1
    error('Function return only one output!');
end

if n < 1966 
    gen = 'O';
elseif n <= 1980
    gen = 'X';
elseif n <= 1999
    gen = 'Y';
elseif n <= 2012
    gen = 'Z';
else
    gen = 'K';
end
