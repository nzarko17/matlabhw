function res = sparse_array_out(A, filename)
%SPARSE_ARRAY_OUT   Write dimensioned array in binary
%   RETVAL = SPARSE_ARRAY_OUT(A,'FNAME') writes the number of
%   dimensions of A, then a list of the dimensions, 
%   and then the indices of non zeros elements of A 
%   followed by the element it self into the file named
%   'fname', encoded as a doubles. This function is used 
%   to store sparse arrays into a file.
fid = fopen(filename,'w+');
if fid < 0
    fprintf('error opening file %s\n', filename);
    res = false;
    return;
end
dims = uint32(size(A));
%Find the non zeros elements and return their indices
%[row, col] and their values nz.
[row, col, nz] = find(A);
%create the header of the file.
% Stores the number of rows ,columns, and the number of
% non zeros element of the array
header = [dims uint32(length(nz))];
%fwrite(fid,length(dims),'uint32');
fwrite(fid,header,'uint32');
for ii = 1:length(row)
    fwrite(fid,row(ii),'uint32');
    fwrite(fid, col(ii), 'uint32');
    fwrite(fid, nz(ii), 'double');
end
%fwrite(fid, nz, 'double');
fclose(fid);
res = true;