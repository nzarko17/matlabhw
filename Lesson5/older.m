function res = older(y1,m1,d1,y2,m2,d2)
%convert dates to serial number date (days)
dn1 = datenum([ y1 m1 d1]);
dn2 = datenum([y2 m2 d2]);

if dn1 < dn2 
    res = 1;
elseif dn1 == dn2 
    res = 0;
else
    res = -1;
end
