
#include <iostream>
#include <stdio.h>
using namespace std;

void numtoroman(int);

int main()
{
    //numtoroman(59);
    //numtoroman(57);
    //numtoroman(55);
    //numtoroman(53);
    //numtoroman(51);
    //numtoroman(49);
    //numtoroman(47);
    for ( int i= 1; i<=100; i++)
		numtoroman(i);
    
}

void numtoroman(int num)
{
    int i=0;
    int count=0;
    int temp=num;
    int rev=0;
    while(temp>0)
    {
        rev=rev*10+temp%10;
        temp=temp/10;
        count++;
    }
    int val=0;
    char sym[7]={'I','V','X','L','C','D','M'};

    printf("\n%d : ",num);
    while(rev>0)
    {
        val=rev%10;
        rev/=10;
        switch(count)
        {
            case 1:i=0;break;
            case 2:i=2;break;
            case 3:i=4;break;
            case 4:i=6;break;
        }
        switch(val)
        {
            case 1:printf("%c",sym[0+i]);break;
            case 2:printf("%c%c",sym[0+i],sym[0+i]);break;
            case 3:printf("%c%c%c",sym[0+i],sym[0+i],sym[0+i]);break;
            case 4:printf("%c%c",sym[0+i],sym[1+i]);break;
            case 5:printf("%c",sym[1+i]);break;
            case 6:printf("%c%c",sym[1+i],sym[0+i]);break;
            case 7:printf("%c%c%c",sym[1+i],sym[0+i],sym[0+i]);break;
            case 8:printf("%c%c%c%c",sym[1+i],sym[0+i],sym[0+i],sym[0+i]);break;
            case 9:printf("%c%c",sym[0+i],sym[2+i]);break;
            case 0:printf("%c",sym[2+i]);break;
        }
        count--;
    }
}
