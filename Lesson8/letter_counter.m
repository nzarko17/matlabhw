function ss = letter_counter(filename)
fid = fopen(filename,'rt');
if fid < 0
    fprintf('error opening file %s\n\n', filename);
    ss = -1;
    return;
end
% Read file as a set of strings, one string per line:
oneline = fgets(fid);
ss = sum(isletter(oneline));

while ischar(oneline)
%fprintf('%s',oneline) % display one line
oneline = fgets(fid);
ss = ss + sum(isletter(oneline));
end
%fprintf('\n');
fclose(fid);
