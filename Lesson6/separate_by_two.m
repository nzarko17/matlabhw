function [vev, vod] = separate_by_two(A)
vev = A(~mod(A,2));
vod = A(mod(A,2)==1);
vev = vev';
vod = vod';
