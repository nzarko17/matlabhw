function res = sparse_array_in(filename)
%SPARSE_ARRAY_IN   Read dimensioned array in binary
%   A = SPARSE_ARRAY_IN('FNAME') reads from 
%   a file of doubles named 'fname' into an sparse array. The 
%   file must contain the number of dimensions of the 
%   array, the number of non zeros elements and then
%   the pair of indices of non zeros and the value of
%   non zero element.
fid = fopen(filename,'r');
if fid < 0
    fprintf('error opening file %s\n', filename);
    res = [];
    return;
end

n = fread(fid,3,'uint32');
dims = n(1:2);
% Initialize res
res = zeros(dims(1),dims(2));
for ii = 1:n(end)
    indx = fread(fid,2,'uint32');
    value = fread(fid, 1, 'double');
    res(indx(1), indx(2)) = value;
end
% A = fread(fid,'double');
% A = reshape(A,dims');
fclose(fid);