%C=CENSOR(V,STR) Removes lines from V that equals or contains STR
function res = censor(v,str)
l = length(v);
k = 1;
res = {};
for ii = 1:l
    if isempty(strfind(v{ii}, str))
        res{k} = v{ii};
        k = k+ 1;
    end
end

