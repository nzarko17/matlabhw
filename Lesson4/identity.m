function sq = identity(n)
rz = zeros(n);
i = 1:(n+1):n^2;
rz(i) = 1;
sq=rz;
end