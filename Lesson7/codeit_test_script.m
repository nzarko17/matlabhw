str = 'The numbers are odd';
let = 'a':'z'; letC =  'A':'Z';
start = int8(let(1));
last = int8(let(end));
lastC = int8(letC(end));
startC = int8 (letC(1));
range = 25;
res = int8(zeros(1,length(str)));
for ii = 1:length(str)
    if int8(str(ii)) >= start && int8(str(ii)) <= last
        d = int8(str(ii)) - start;
        res(ii) = let(end-d);
    end
    if int8(str(ii)) >= startC && int8(str(ii)) <= lastC
        d = int8(str(ii)) - startC;        
        res(ii) = letC(end-d);
    end    
end