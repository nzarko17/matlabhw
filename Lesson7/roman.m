function res = roman(roman_rep)

r = 'IVX';
v = [1 5 10];

l = length(roman_rep);

% first check the integrity of roman_rep
% Only three consecutive symbols can be the same (IIII or VIIII are illegal,
% but IV and IX are fine), and a subtractive notation cannot be
% followed by an additive one using the same symbols making strange combinations, such as IXI
% for 10 or IXX for 19, illegal also.
% First check for valid letters.
for ii = 1:l
    if isempty(strfind(r, roman_rep(ii)))
        res = uint8(0);
        return;
    end
end
ii = 1;
if l > 5
    res = uint8(0);
    return;
elseif l == 1
    res = uint8(v(strfind(r, roman_rep)));
    return;
else
    t = size(strfind(roman_rep, 'I')); 
    if t(2) >= 4 || t(1) >= 4
        res =uint8(0);
        return;
    end
    t= size(strfind(roman_rep, 'V'));
    if t(1) >= 2 || t(2) >= 2
        res = uint8(0) ;
        return;
    end;
    tv = strfind(roman_rep,'V');
    tx = strfind(roman_rep,'X');
    if ~isempty(tv) && ~isempty(tx) && tv(1) < tx(1)
        res = uint8(0);
        return;
    end
    if l >=3
        while ii <= l -2
            if roman_rep(ii) < roman_rep(ii+1) && roman_rep(ii+1) >= roman_rep(ii+2)
                res = uint8(0);
                return
            end
            if roman_rep(ii) == 'I' && roman_rep(ii+1) == 'I' && roman_rep(ii+2) ~= 'I'
                res = uint8(0);
                return;
            end;
            ii = ii + 1;
        end
    end
end

% Lets do the calculation
ii = 1;
res = uint8(0);
while ii <= l - 1
    if roman_rep(ii) < roman_rep(ii+1)
        res = uint8(res + v(strfind(r, roman_rep(ii+1))) - v(strfind(r, roman_rep(ii))));
        ii = ii + 2;
    else
        res = uint8(res + v(strfind(r, roman_rep(ii))));
        ii = ii + 1;
        if ii == l
            res = uint8(res + v(strfind(r, roman_rep(ii))));
        end
    end
    
end

if res > uint8(20) 
    res = uint8(0);
end;




        