function mo = moving_average(x)
persistent vec;
if isempty(vec)
    vec(1) = x;
else
    vec = [vec x];
end

if length(vec) > 25
    vec(1) = [];
end

mo = mean(vec);
