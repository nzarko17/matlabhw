function res = divvy(A,k)
res = A;
res(mod(res,k)>0) = res(mod(res,k)>0) * k;
