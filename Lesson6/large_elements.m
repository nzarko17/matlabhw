function res = large_elements(X)
res = [];
[nrows, ncols] = size(X);
for ii=1:nrows
    for jj = 1:ncols
        if (ii + jj) < X(ii,jj)
            res = [res; ii jj];
        end
    end
end
