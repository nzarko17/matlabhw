function X = odd_index(M)
X = M(1:2:end, 1:2:end);
end
