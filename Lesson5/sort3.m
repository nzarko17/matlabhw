function vec = sort3(a,b,c)
% Find the minimum element and its position
% on vector tmp
tmp = [a b c]
m = a;
pos = 1;
if b < m
    m = b;
    pos = 2;
end
if c < m
    m = c;
    pos = 3;
end
vec(1) = m;
% now remove the minimum element from tmp
tmp(pos) = [];
% Compare the other two element and make the good choise.
m = tmp(1);
if tmp(2) < m
    m = tmp(2);
    vec(2) = m;
    vec(3) = tmp(1);
else
    vec(2) = tmp(1);
    vec(3) = tmp(2);
end


