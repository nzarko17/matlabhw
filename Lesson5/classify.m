function res = classify(x)
if ndims(x) > 2
    error('The function parameter must have no more than 2 dimensions!');
end
[nrows, ncols] = size(x);

if nrows == 0 || ncols == 0
    res = -1;
elseif nrows ==1 && ncols == 1
    res = 0;
elseif (nrows == 1 && ncols > 1) || (nrows > 1 && ncols == 1)
    res = 1;
else 
    res = 2;
end
