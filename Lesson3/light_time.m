function [tm, km] = light_time(v)
u = 3 * 10^5 * 60 / 1.609;
tm = v / u;
km = 1.609 .* v;
end