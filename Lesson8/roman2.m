function retval = roman2(rn)
%ROMAN2(rn) convert the Roman representation of rn ton an integer.
%retval = ROMAN2(rn) retval contains the integer value of rn if rn is a
%valid roman number representation as it is explained at
% http://en.wikipedia.org/wiki/Roman_numerals

%First load the file containning all the first 399 
% roman numbers and store its value in roman_number cell.
fid = fopen('roman_numbers.txt','rt');
if fid < 0
    error('error opening file %s\n', filename);
end

roman_number = cell(399,1);
% Read file as a set of strings, one string per line:
lineno=1;
oneline = fgets(fid);
while ischar(oneline)
    %fprintf('%s',oneline) % display one line
    roman_number{lineno}=oneline(1:(end-1));
    oneline = fgets(fid);
    lineno = lineno + 1;
end
%fprintf('\n');
fclose(fid);

%str = cellstr(rn);
num = 1;
found = false;
while (num <= 399) && (found == false)
    str = char(roman_number{num,1});
    if strcmp(str,rn) == 1
        found = true;
        break;
    end;
    num = num + 1;
end;

if found == true
    retval = uint16(num);
else
    retval = uint16(0);
end

