function [row, col, numrows, numcols, summa] = maxsubsum(A)
% MAXSUBSUM Finds the submatrix that has the maximum sum.
%   [row, col, numrows, numcols, summa]=MAXSUBSUM(A)
%   A : is the input matrix
%   row, col : spesify the indices of the top left corner of the sub matrix
%   with the maximum sum
%   numrows, numcols are its dimensions
%   summa is the sum of its elements.
%Uses the external function kadane
%see also kadane

%initialize first
summa = -inf;
finalLeft = 0; finalRight= 0;
finalTop = 0; finalBottom = 0;
%row = 0; col = 0; numrows = 0; numcols = 0;
[ROW, COL] = size(A);
for left=1:COL
    temp = zeros(ROW,1);
    for right=left:COL
        for ii=1:ROW
            temp(ii) = temp(ii) + A(ii,right);
        end
        [ssum,start,finish] = kadane(temp);
        if ssum > summa
            summa = ssum;
            finalLeft = left;
            finalRight = right;
            finalTop = start;
            finalBottom = finish;
        end
    end
end

row = finalTop;
col = finalLeft;
[numrows,numcols] = size(A(row:finalBottom,col:finalRight));


