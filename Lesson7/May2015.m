function res = May2015
res = struct('month',{},'date',{}, 'day', {});

days_name = ['Sun'; 'Mon'; 'Tue'; 'Wed'; 'Thu';'Fri';'Sat'];

cal = calendar(2015,5);
[nrows, ncols] = size(cal);

k = 0;
for ii = 1:nrows
    for jj = 1:ncols
        if cal(ii,jj) == 0
            continue;
        else
         res(k+1).month = 'May';
         res(k+1).date = cal(ii,jj);
         res(k+1).day = days_name(jj, :);
         k = k + 1;
        end
    end
end
