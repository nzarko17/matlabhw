function w = replace_me(v,a,b,c)
l = length(v);
fprintf('Number of arguments : %d\n', nargin);

if nargin < 3
    c = 0;
    b = 0;
elseif nargin < 4
    c = b;
end

tmp = [];

for ii=1:l
    if v(ii) == a
        tmp = [tmp b c];
    else
        tmp = [tmp v(ii)];
    end
end
w= tmp;
