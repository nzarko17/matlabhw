function board=checkerboard(n,m)
tmp = zeros(n,m);
i=1:2:n;
j=1:2:m;
tmp(i,j) = 1;
i=2:2:n;
j = 2:2:m;
tmp(i,j) = 1;
board = tmp;
end