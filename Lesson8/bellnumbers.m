function B = bellnumbers(n)
% BELLNUMBERS(N) Return the first n Bell numbers.
%   B = BELLNUMBERS(N) C is a row vector containning the
%   first n Bell numbers.
%   see also  bell .

if n <= 0
    error('Function argument must be a positive integer');
end
B = zeros(1,n);

for nn=0:n
    BN = 0;
    for k=0:nn
        BN = BN + nstir2k(nn,k);
    end
    B(nn+1) = BN;
end

%Return 
B = B(1:n);
