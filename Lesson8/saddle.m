function indices = saddle(M)
[n,m]=size(M);

%Find the max elements of each row and store its indices 
% at rmax array
rmax_ind = zeros(n,2);
%max element for each row
rmax_el = zeros(n,1);

for ii = 1:n
    rmax_el(ii) = M(ii,1);
    %rmax_ind(ii,1) = ii;
    %rmax_ind(ii,2) = 1;
    for jj = 1:m
        if M(ii,jj) > rmax_el(ii)
            rmax_el(ii) = M(ii,jj);
        end
    end
end

%check if each row max element is equal with another element of the same
%row
k = 1;
for ii = 1:n;
    for jj = 1:m
        if rmax_el(ii) == M(ii,jj)
            rmax_ind(k,1) = ii;
            rmax_ind(k,2) = jj;
            k = k + 1;
        end
    end
end
%Find the min elements of each column and store its 
%value at cmin_el array.
cmin_ind = zeros(m,2);
cmin_el = zeros(m,1);
for jj = 1:m
    cmin_el(jj) = M(1,jj);
%     cmin_ind(jj,1) = 1;
%     cmin_ind(jj,2) = jj;
    for ii = 1:n
        if M(ii,jj) < cmin_el
            cmin_el(jj) = M(ii,jj);
%             cmin_ind(jj,1) = ii;
%             cmin_ind(jj,2) = jj;
        end
    end
end

k = 1;
for jj=1:m
    for ii=1:n
        if cmin_el(jj) == M(ii,jj)
            cmin_ind(k,1) = ii;
            cmin_ind(k,2) =jj;
            k = k+ 1;
        end
    end
end

%rmax_ind
temp = cmin_ind(:,1);
cmin_ind(:,1) = cmin_ind(:,2);
cmin_ind(:,2) = temp;
%cmin_ind

indices = [];
k=1;
for ii=1:n
    for jj = 1:m
        if isequal(rmax_ind(ii,:),cmin_ind(jj,:))
            indices(k,:)=rmax_ind(ii,:);          
            k=k+1;
        end
    end
end


