function res = myprime(n)
r1 = true;

if n <= 1
    res = false;
    return;
elseif n <= 3
    res = true;
    return;
elseif rem(n,2) == 0 || rem(n,3) == 0
    res = false;
    return;
end

ii = 5;
while ii^2 <= n
    if (rem(n,ii) == 0) || (rem(n, (ii+2)) == 0 )
        r1 = false;
        break;
    end
    ii = ii + 6;
end
res = r1;
