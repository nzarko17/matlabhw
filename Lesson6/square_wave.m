function res = square_wave(n)
t = 0:((4*pi)/1000):4*pi;
res = zeros(1,1001);
for ii = 1:length(t) 
    ss = 0;
    for k=1:n
       ss =ss + sin((2*k -1)*t(ii)) /(2*k - 1);
    end
    res(ii) = ss;    
end


