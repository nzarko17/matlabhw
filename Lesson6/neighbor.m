function res = neighbor(v)
l = length(v);
if isvector(v) && l >= 2
    res = zeros(1,l-1);
    for ii=1:(l - 1)
        res(ii) = abs(v(ii) - v(ii+1));
    end
else
    res = [];
end
