function grade = letter_grade(score)
if nargin < 1 
    error('Function takes only one argument!');
end
if score < 0 || score ~= fix(score)
    error('Function argument must be a positive integer!');
end
if score < 61
    grade = 'F';
elseif score <= 70
    grade = 'D';
elseif score <= 80
    grade = 'C';
elseif score <= 90
    grade = 'B';
else
    grade = 'A';
end
