function sums = halfsum(A)
if ~ismatrix(A) 
    error('Function argument must be at most 2 dimensional matrix!');
end
sums = 0;
[nrows, ncols] = size(A);

for ii=1:nrows
    for jj = 1:ncols
        if ii == jj || jj > ii
            sums = sums + A(ii,jj);
        end
    end
end
