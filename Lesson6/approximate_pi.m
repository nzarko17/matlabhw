function [res, k] = approximate_pi(delta)
k = 0;
res = sqrt(12);
while abs(res - pi) >= delta
    k = k + 1;
    res = res + sqrt(12) * ((-3)^(-k)) / (2 * k + 1);
end
