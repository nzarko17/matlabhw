%DIAL Translate a mixed phone number 
%RES = DIAL(STR) Translate STR to digital phone number
function res = dial(str)
%indicates if an error occurs.
error_ = 0;

num_list = cell(8,2);
k = 2;
%Initialize the num-string table
string_list = ['ABC'; 'DEF'; 'GHI'; 'JKL'; 'MNO'; 'PRS'; 'TUV'; 'WXY'];
for ii = 1:8
    num_list{ii,1} = k;
    num_list{ii,2} = string_list(ii,:);
    k = k + 1;
end
%Show the results
%num_list

%some other usefull constants
SHARP = '#';
MUL = '*';
SPACE = ' ';
DASH = '-';
LP = '(';
RP = ')';

%Now we are ready to start. For each character in str and str1 
% check if its exist in any of the above and do the convertion
% If not the an error arised and stop the execution.
str_len = length(str);
res = str;
%str = str_ok;
for ii = 1:str_len
    if str(ii) == SHARP || str(ii) == MUL || isstrprop(str(ii),'digit') || str(ii)==SPACE %don't change anything, go to next loop
        continue;
    elseif str(ii) == DASH || str(ii) == LP || str(ii) == RP
        res(ii) = SPACE;
    elseif str(ii) >= 'A' && res(ii) <= 'Y' && str(ii) ~= 'Q' && str(ii) ~= 'Z'
        for jj = 1:8
            if isempty(strfind(num_list{jj,2}, str(ii)))
                continue;
            else
                res(ii) = num2str(num_list{jj,1});
            end
        end
    else
        fprintf('Error at position %d . Unknown character or symbol\n',ii);
        error_ = 1;
        res = [];
        break;
    end
end