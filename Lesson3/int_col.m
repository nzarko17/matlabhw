function v = int_col(n)
x = 1:n;
v = [x(end), x(1:(end-1))]' ;
end