function [mt,s] = mtable(n,m)
i = 1:n;
j = 1:m;
[a,b] = meshgrid(i,j);
mt = a.*b;
mt = mt';
ts = sum(mt);
s = sum(ts);
end