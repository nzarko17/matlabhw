function [maxSum, maxStartIndex, maxEndIndex] = kadane(A)
%KADANE(A) Finds the maximum (positive) sum of contiguous elements in
%vector A.
%[maxSum,maxStartIndex,maxEndIndex]=KADANE(A) 
% MAXSUM : the maximum sum.
% MAXSTARTINDEX : the index of the first element of the sum.
% MAXENDINDEX : the index of the last element of the sum

% SEE kadane algorithm :
% http://www.algorithmist.com/index.php/Kadane's_Algorithm 
% http://en.wikipedia.org/wiki/Maximum_subarray_problem


% Begin the kadane algorithm for A
maxSum = -inf; maxStartIndex = 0; maxEndIndex = 0;
currentMaxSum = 0;
currentStartIndex = 1;
for currentEndIndex = 1:length(A)
    currentMaxSum = currentMaxSum + A(currentEndIndex);
    if currentMaxSum > maxSum 
        maxSum = currentMaxSum;
        maxStartIndex = currentStartIndex;
        maxEndIndex = currentEndIndex;
    end
    
    if currentMaxSum < 0
        currentMaxSum = 0;
        currentStartIndex = currentEndIndex + 1;
    end
end

end

