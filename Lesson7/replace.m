%C=REPLACE(V,C1,C2) replace C1 with C2 in cell vector V
function res = replace(vec,c1,c2)
res = vec;

l = length(res);
for ii = 1:l;
    res{ii}(res{ii} == c1) = c2;
end
