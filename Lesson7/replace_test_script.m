vec = {'The function takes as parameter an integer',
'between 20 and 100 and return square of it.'}
c1 = 'a'; c2 = 'w';

l = length(vec);
for ii = 1:l;
    vec{ii}(vec{ii} == c1) = c2;
end

vec
    