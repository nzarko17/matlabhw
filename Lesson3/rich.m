function d = rich(v)
d = 0.01 * v(1) + 0.05 * v(2) + 0.1 * v(3) + 0.25*v(4);
end
