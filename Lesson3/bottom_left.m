function a = bottom_left(N, n)
a = N((end - n + 1):end, 1:n);
end
