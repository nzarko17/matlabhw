function A = bell(n)
%BELL(N) return an n by n  upper triangular array containning the bell numbers
%   A = BELL(N) A is an upper triangular n by n array with 
%   the first n Bell numbers as its elements.

if n <= 0 || ceil(n) ~= n
    A = [];
    return;
end

%initialize the 
A = zeros(n);
B = bellnumbers(n + 1);
A(1,:) = B(2:end);
A(:,1) = B(1:(end-1));

for jj=1:(n-1)
    for ii=2:(n-jj)
        A(ii,jj+1) = A(ii,jj) + A(ii+1,jj);
    end
end
